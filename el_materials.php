
<? /*
* При возвращении на страницу ЛК получаем ответ от платежной системы *
* об оплате конкретного материала по определенному счету,
* и ставим этому счету статус «Оплачен» 
* очень старый код
*/
?>
		<? if (!empty($_REQUEST['pay_el_books']) && !empty($_REQUEST['orderId'])) {
			/* Получаем ID в мерчанте счета из ответа платежной системы */
			$order_id = trim(strip_tags($_REQUEST['orderId']));
			/* Находим по нему этот счет */
			$ar_select_el_books = Array("ID", "IBLOCK_ID", "PROPERTY_*");
			$ar_filter_el_books = Array("IBLOCK_ID"=>47, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y", "PROPERTY_orderId"=> $order_id);
			$res = CIBlockElement::GetList(Array(), $ar_filter_el_books, false, Array("nPageSize"=>3), $ar_select_el_books);
			if($ob = $res->GetNextElement()){ 
				$arFields = $ob->GetFields();  
				$arProps = $ob->GetProperties();
				$bill_id = $arFields['ID'];

				/* Ставим статус «Оплачен» */
				$prop['PAIDED'] = 120;
				CIBlockElement::SetPropertyValuesEx($bill_id, 47, $prop);
			}
		} ?>

<? /* Выводим вкладку «Электронный раздаточный материал» у пользователя, 
* для которого открыт хотя бы один материал *
*/ ?>
				<?
				$index_el_books = 'N';
				$rs_user_for_el_books = CUser::GetByID($USER->GetID());
				$ar_user_for_el_books = $rs_user_for_el_books->Fetch();

				$arSelect = Array("ID", "IBLOCK_ID", "PROPERTY_*");
				$arFilter = Array("IBLOCK_ID"=> 64, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
				$res = CIBlockElement::GetList(Array("SORT"=>"ASC"), $arFilter, false, Array("nPageSize"=>100), $arSelect);
				while($ob = $res->GetNextElement()){ 
					$arFields = $ob->GetFields();  
					$arProps = $ob->GetProperties();
	 
					$groops = $arProps["GROUP"]["VALUE"];
					foreach ($groops as $groop) {
						$groop = trim($groop);
						if (intval($ar_user_for_el_books[$groop]) == 1) {
							$index_el_books = 'Y';
							break;
						}
					}
				}
				if ($index_el_books == 'Y') {
				?>
				<li style="list-style-type:none;"><a href="#el_books">Электронный раздаточный материал</a></li>
				<? } ?>
		

<? /* Выводим этому пользователю материалы, которые ему доступны */ ?>
		<? if ($index_el_books == 'Y') { ?>
		<div style="padding:20px;" id="el_b">
			<h2><a name="what" id="el_books"></a></h2>
			<?
			$ar_select_el_books = Array("ID", "IBLOCK_ID", "NAME", "PREVIEW_TEXT", "PREVIEW_PICTURE", "PROPERTY_*");
			$ar_filter_el_books = Array("IBLOCK_ID"=> 64, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
			$res_el_books = CIBlockElement::GetList(Array("SORT"=>"ASC"), $ar_filter_el_books, false, Array("nPageSize"=>100), $ar_select_el_books);
			while($ob_el_books = $res_el_books->GetNextElement()){ 
				$ar_fields_el_books = $ob_el_books->GetFields();  
				$ar_props_el_books = $ob_el_books->GetProperties();

				$id = intval($ar_fields_el_books["ID"]);
				$name = $ar_fields_el_books["NAME"];
				$text = $ar_fields_el_books["PREVIEW_TEXT"];
				$src = CFile::GetPath($ar_fields_el_books["PREVIEW_PICTURE"]);

				$price = intval($ar_props_el_books["PRICE"]["VALUE"]);
				$files = $ar_props_el_books["FILES"]["VALUE"]; 

				$groops = $ar_props_el_books["GROUP"]["VALUE"];
				foreach ($groops as $groop) {
					$groop = trim($groop);
					if (intval($ar_user_for_el_books[$groop]) == 1) {
						$user = intVal($USER->GetID());
						$bill_id = 0;
				
						$ar_select_el_books = Array("ID", "IBLOCK_ID", "PROPERTY_*");
						$ar_filter_el_books = Array("IBLOCK_ID"=>47, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y", "PROPERTY_USER"=> $user, "PROPERTY_REQ"=> $id, "PROPERTY_PAIDED"=> 120);
						$res = CIBlockElement::GetList(Array(), $ar_filter_el_books, false, Array("nPageSize"=>3), $ar_select_el_books);
						if($ob = $res->GetNextElement()){ 
							$arFields = $ob->GetFields();  
							$arProps = $ob->GetProperties();
							$bill_id = $arFields['ID'];
						}
				?>

				<div class="el_books">
					<div class="img">
						<div class="block_flow"><div class="block_align"><img src="<?=$src?>" alt="<?=$name?>" title="<?=$name?>" valign="absmiddle"></div></div>
					</div>
					<div class="content">
						<div class="title">
							<?=$name?>
						</div>
						<div class="text">
							<? echo htmlspecialcharsBack($text); ?>
						</div>
						<? if ($price > 0 && $bill_id == 0) { ?>
						<div class="price_el_bs active" data-summ="<?=$price?>" data-elm="<?=$id?>">
							Купить за <span><?=$price?></span> р.
						</div>
						<? } else { ?>
						<div class="file_el_bs">
							<? $count_file = 0;
							foreach ($files as $file) { 
								$count_file++;

								$path = CFile::GetPath($file);
								$ext =  end(explode(".", $path));
 
								/* Рачет размера. Сделан, но не выведен, так как в макете не оказалось места 
								$arFile=CFile::MakeFileArray($_SERVER["DOCUMENT_ROOT"].$path);
								$size=$arFile["size"];
								$sizes = array('B', 'Kb', 'Mb', 'Gb', 'Tb', 'Pb', 'Eb', 'Zb', 'Yb'); 
								for ($i=0; $size > 1024 && $i < count($sizes) - 1; $i++) $size /= 1024; */
								?>
								<a href="<?=$path?>" download><?=$ext?><?/* echo ' ('.round($size,$round)." ".$sizes[$i].')'*/; ?></a>
								<? if ($count_file > 1) {
									echo ' ';
								}
							} ?>
						</div>
						<? } ?>
					</div>
				</div>
				<?	break;
					}
				}?>
			<?
			
			}
			?>
		</div>
		<? } ?>
<?
}	
?>		
        <?if($USER->IsAdmin()){?>
            <div style="padding:20px;" id="admi">
                <h2><a name="what" id="adminn"></a></h2>
                <form action="<?=$APPLICATION->GetCurPage()?>" method="POST" name="curform">
					<input type="text" class="typeinput" name="DATE" size="12" style="height:10px;padding:5px;border:1px solid gray;">
					<?=Calendar("DATE", "curform")?>
				</form>
                <table style="width:100%;" class="trans">
                    <tr>
                        <td width="5%" style="font-weight: bold;">№ Ордера</td>
                        <td width="20%" style="font-weight: bold;">Пользователь</td>
                        <td width="35%" style="font-weight: bold;">Тренинги</td>
                        <td width="10%" style="font-weight: bold;">Сумма</td>
                        <td width="10%" style="font-weight: bold;">Дата</td>
                    </tr>
                    <?
                    $resPay = CIBlockElement::GetList(Array(),Array('IBLOCK_ID'=>47,'ACTIVE'=>'Y','PROPERTY_PAIDED'=>120),false,false,Array('*','PROPERTY_PRICE','PROPERTY_TRAINING','PROPERTY_USER'));
                    ?>
                <?while($trans=$resPay->fetch()):?>
                        <?
                    if(is_array($trans['PROPERTY_TRAINING_VALUE']))
                    {
	                    foreach($trans['PROPERTY_TRAINING_VALUE'] as $train)
	                    {
		                    $ccc = CIBlockElement::GetByID($train)->fetch();
		                    $names[] = $ccc['NAME'];
		                    $elms[] = $ccc['ID'];
	                    }
                 	    $names = implode("<BR>",$names);
                    }
                    else
                    {
	                    $ccc = CIBlockElement::GetByID($trans['PROPERTY_TRAINING_VALUE'])->fetch();
	                    $names = $ccc['NAME'];
	                    $elms[] = $ccc['ID'];
                    }
                    ?>
                    <tr>
                        <?$US = $USER->GetByID($trans['PROPERTY_USER_VALUE'])->fetch();?>
                        <td><?=$trans['ID']?></td>
                        <td><?=$US['NAME'].' '.$US['LAST_NAME']?></td>
                        <td><?=$names?></td>
                        <td><?=$trans['PROPERTY_PRICE_VALUE']?> руб.</td>
                        <td><?=$trans['DATE_CREATE']?></td>
                        </tr>
                    <?endwhile;?>
                </table>
            </div>
        <?}?>
	</div>	

</div>
